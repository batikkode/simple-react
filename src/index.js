import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Calculator from './Calculator';
import Todo from './Todo';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Calculator />,	document.getElementById('root'));
ReactDOM.render(<Todo />,	document.getElementById('mytask'));
registerServiceWorker();
