import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';

class Calculator extends React.Component {
	/* inisialisasi state */
	constructor(){
		super();
		this.state = {
			nilai1: null,
			nilai2: null,
			operator: null,
			hasil: null
		}
	}
	
	/* memproses perhitungan kalkulator */
	handleSubmit(e) {
		e.preventDefault();
		const nilai1 = parseInt(this.refs.nilai1.value);
		const nilai2 = parseInt(this.refs.nilai2.value);
		const operator = this.refs.operator.value;		
		
		//proses perhitungan
		var hasil;
		switch(operator) {
			case '+':
				hasil = nilai1 + nilai2;
				break;
			case '-':
				hasil = nilai1 - nilai2;
				break;	
			case '*':
				hasil = nilai1 * nilai2;
				break;		
			case '/':
				hasil = nilai1 / nilai2;
				break;			
		}
		
		this.setState({ nilai1, nilai2, operator, hasil });	
		
		// kosongkan input field
		this.refs.nilai1.value = null;
		this.refs.nilai2.value = null;
		
	}
	/* menampilkan hasil */
	renderHasil() {
		const {nilai1, nilai2, operator, hasil} = this.state;
		// cek jika state hasil ada
		if (this.state.hasil) {
			return (
				<div>
					<br />
					<p className="alert alert-success">
						{nilai1+operator+nilai2+'='+hasil}
					</p>
				</div>
			);
		}
		
	}
	/* untuk merender DOM pada browser */
	render() {
		return (
			<div className="App">
			 <header className="App-header">
			  <img src={logo} className="App-logo" alt="logo" />
			  <h1 className="App-title">Aplikasi Kalkulator Sederhana</h1>
			</header>
			<p className="App-intro">
				<br />
				<form onSubmit={this.handleSubmit.bind(this)} class="form-inline">
					<label>Nilai:</label>
					<input type="number" ref="nilai1" class="form-control mb-2 mr-sm-2" />
					<select ref="operator" className="form-control">
						<option value="+">+</option>
						<option value="-">-</option>
						<option value="*">*</option>
						<option value="/">/</option>
					</select>
					<input type="number" ref="nilai2" className="form-control" />
					<br />
					<br />
					<button className="btn btn-primary">Lihat Hasil</button>
				</form>
			</p>	
				{this.renderHasil()}
			</div>	
		);
	}
}

export default Calculator;