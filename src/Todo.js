import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Todo extends React.Component {
	//mendefiniskan state
	constructor() {
		super();
		this.state = {
			todos: []
		}
		//bind
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	
	//menambahkan (push) todo terbaru
	handleSubmit(e){
		e.preventDefault();
		var task = this.refs.task.value;
		
		this.state.todos.push({task, id: +new Date});
		
		this.setState({
			todos: this.state.todos
		});
		// reset field
		this.refs.task.value = '';		
	}
	//untuk menampilkan hasil
	renderHasil() {
		// if not empty
		if(this.state.todos.length){
			var todos = this.state.todos.map((todo, i) => {
				return <li key={i}>{todo.task}</li>
			});
			return (
				<ul className="alert alert-success">{todos}</ul>
			);
		}
		return null		
	}
	
	render() {
		return (
			<div className="App">
				<h3>My Tasks : </h3>			  
				  <form onSubmit={this.handleSubmit} class="form-inline" >
					<input type="text" className="form-control" ref="task" />
					<br /><br />
					<button className="btn btn-primary">Add Todo</button>
				  </form>
				  <br />
				 <div class="col-md-3">{this.renderHasil()}</div>
			</div>
		);
	}
}

export default Todo;